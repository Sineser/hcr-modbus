#Hanwha hcr modbust api
this is a test API for communication and controlling robot via ethernet communication
project includesscript file for robot controller

##Change log

v 1.0
* initial files added

v 1.1
* robot script fixies
* communication controler as Process object
* Created Robot class in hcrRobot script, first handlers added

v 1.2
* controler coordinate recive
* robot script - coordinates sanding

v 1.3
* robot intarface GetPose function
* robot intarface Deconstructor and Disconnection fucntions
* robot script Pose sending

v 1.4 
* robot controller LinMove pos sending to robot
* Robot controller data sending algorithm
* robot script position receving ** do not checked with negative numbers

v 1.5
* robot controller new comands handling
* robot intarface SetSpeed functions, move function block while robot in motion
* robot script SetSpeed request, sygnal on complited movement

v 1.6 
* robot intarface new fucntions:
* GetJointPose, MovePTP, MovePTPRel
* robot controller optimisations for new functions
* robot script Getpose and move Joint functions

**Known issues** setSpeed not working from robot script side 

v 1.7
* robot intarface added GetDigitaInputs, SetDigitalOutputs
* robot controller upadted for new fucntions
* robot script updated for new functions
* setSpeed fix. endless loop in script ** script must be placed outside loops**

v 1.8
* SetToolPower function, Get digital outputs status, and some optimisations

v 1.9 
* print fixies
* MoveLin, MoveLinRel, GetPose updated for using frame parameter
