import ModBus_Encoding
import socket
from multiprocessing import Process, Pipe ,Queue

def Unpuck(byte_data):
    '''
    Gets bytestring as input, unpucking hexdata to list of ints
    '''
    result =[]
    for piece in byte_data:
        result.append(piece)
    return result

def Connect(ip,port):
    '''
        Establish socket server for robot connection on cuurent system
        Ip should be current system ip 
    '''
    
    sock = socket.socket()
    sock.bind((ip,port))
    sock.listen(1)
    conn, addr = sock.accept()    
    return (conn) 

def Reconnect(sock,ip,port):
    '''
        Function that creates socket server object for future use in programm
    '''
    print("reconnect")
    sock.close()
    sock = socket.socket()
    sock.bind((ip,port))
    sock.listen(1)
    conn, addr = sock.accept()
    return (conn)

def ReturnPart(number,fract):
    ''' 
        Convert float to integer int part or decimal part of number
    '''
    if fract:
        return (int(number*10000 %10000))
    else:
        return(int(number))
    


def Send_To_Robot(socket_object,data,command,prefix):
    
    '''
        Send data to robot through socket_object
    '''
    #print ('True D',data)
    if data>=0:
        byteHi = data//256
        byteLo = data - byteHi*256
    else:
        data = 65535 + data
        
        byteHi = data//256
        byteLo = data - byteHi*256
    #print ('final D',data,'H',byteHi,'L',byteHi)    

        
    socket_object.send(prefix+chr(command).encode()+chr(1).encode()+
                       ModBus_Encoding.codeMap[byteHi]+
                       ModBus_Encoding.codeMap[byteLo])
#     print('sended',prefix+chr(command).encode()+chr(1).encode()+
#                        ModBus_Encoding.codeMap[byteHi]+
#                        ModBus_Encoding.codeMap[byteLo])
    

def Request_Handler(socket_object):
    '''
    
    '''
    message = socket_object.recv(50)
    #print('data recieved',Unpuck(message))
    #print('message',Unpuck(message[7:]))
    if message:
        '''
            If messege recived from robot
        
        '''
        
        unpucked_data =Unpuck(message[7:])

        if len(unpucked_data) <8:
            '''
                If the unpucked request corresponds to send data to robot
            '''
            command,adressByteOne,adressByteTwo,s,d= unpucked_data
            true_adress = adressByteOne*256+adressByteTwo
            
        if len(unpucked_data) ==8:
            command =unpucked_data[0]
            true_adress = unpucked_data[1]*256+unpucked_data[2]
            true_data = unpucked_data[6]*256+unpucked_data[7]
            #print(type(true_data))
            if true_data>50000:
                true_data = true_data - 65536
                #print ('tt',tt)
        
            
        
        if command ==4:
            #print('position request')            
            return ({'command':command,'adress':true_adress,'data':0,'prefix':message[0:7]})
        
        if command ==3:
            print('output from robot, adress: ',true_adress,s,d)
            #socket_object.send(message)
            return({'command':command,'adress':true_adress,'data':true_data,'prefix':message[0:7]})
        
        if command == 16:
            to_send = message[0:5]+chr(6).encode()+message[6:12]
            #print('data recieved at adress: ',true_adress,' data recieved: ',true_data)
            #print('data to send', Unpuck(to_send))
            socket_object.send(to_send)
            #print(true_data)
            return({'command':16,'adress':true_adress,'data':true_data,'prefix':message[0:7]})
    else:
        print ('No response from robot')
        return(0)


def Controller(ip,port,pipe_marker,data_marker,pipe_output):
    

    keys = ('x','y','z','rx','ry','rz')
    j_keys = ('j1','j2','j3','j4','j5','j6')
    current_pose = {
                'x': 0, 'y': 0, 'z': 0,
                'rx': 0, 'ry': 0, 'rz': 0}
    robot_conection = Connect(ip,port)
    request_type = 0
    move_command_type = 0
    move_command = {
                'x': 0, 'y': 0, 'z': 0,
                'rx': 0, 'ry': 0, 'rz': 0,
                'acc':0, 'vel':0
               }
    
    while True:
        if not pipe_marker.empty():
            a = pipe_marker.get()
            if a ==1:
                input_data = pipe_output.recv()
                move_command = input_data[2]
                move_command_type = input_data[1]
                request_type = input_data[0]
            if a == 2:
                input_data = pipe_output.recv()
                request_type = input_data[0]
                data = input_data[1]
            
        else:
            request_type = 0
            move_command_type = 0
             
        res = Request_Handler(robot_conection)
        if res:
            if res['command'] == 4:
                if res['adress'] == 1:
                    Send_To_Robot(robot_conection, request_type, res['command'], res['prefix'])  
                elif res['adress'] == 2:
                    Send_To_Robot(robot_conection, move_command_type, res['command'], res['prefix'])
                elif res['adress'] == 4:
                    Send_To_Robot(robot_conection, data, res['command'], res['prefix'])
                elif res['adress'] == 5:
                    Send_To_Robot(robot_conection, data, res['command'], res['prefix'])
                elif res['adress'] == 6:
                    Send_To_Robot(robot_conection, data, res['command'], res['prefix'])
                elif res['adress'] == 50:
                    Send_To_Robot(robot_conection, data, res['command'], res['prefix'])
                elif res['adress'] == 51:
                    Send_To_Robot(robot_conection, data, res['command'], res['prefix'])
                elif res ['adress']>=100:
                    if keys[res['adress']%100 //10] in list(move_command.keys()):
                        Send_To_Robot(robot_conection, ReturnPart(move_command[keys[res ['adress']%100 //10]],res ['adress'] % 10), res['command'], res['prefix'])
                    elif j_keys[res['adress']%100 //10] in list(move_command.keys()):
                        Send_To_Robot(robot_conection, ReturnPart(move_command[j_keys[res ['adress']%100 //10]],res ['adress'] % 10), res['command'], res['prefix'])
                        
                
                #elif res['ardress'] == 13:
                #    Send_To_Robot(robot_conection, move_command['acc'], res['command'],res['prefix'])
                #elif res['ardress'] == 14:
                #    Send_To_Robot(robot_conection, move_command['vel'], res['command'],res['prefix'])
            
            elif res['command'] == 16:
                #print('ardess=',res['ardress'], ' data=',res['data'])
                if res['adress'] == 20:
                    data_marker.put(3)
                    #print('move marker')
                if res['adress'] == 52:
                    pipe_output.send(res['data'])
                    data_marker.put(1)
                if res['adress']>=200 and res['adress'] % 10 == 0:
                    current_pose[keys[res['adress'] % 100 // 10]] = res['data']
                elif res['adress']>=200 and res['adress'] % 10 == 1:
                    current_pose[keys[res['adress'] % 100 // 10]] += (res['data']/10000)
                    if (res['adress'] % 100 // 10 == 5):
                        pipe_output.send(current_pose)
                        data_marker.put(1)
                        current_pose = {
                        'x': 0, 'y': 0, 'z': 0,
                        'rx': 0, 'ry': 0, 'rz': 0}
                        
                                    
                #Send_To_Robot(robot_conection, 0,res['command'], res['prefix'])
        else:
            robot_conection = Reconnect(robot_conection, ip, port)
            
                
            

 